package proof.of.concept.assembler;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import proof.of.concept.Application;
import proof.of.concept.controller.ProfileController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ApplicationAssembler {

    private static final String PROFILES_REL = "profiles";

    public ResourceSupport toResource() {
        ResourceSupport applicationResource = new ResourceSupport();

        applicationResource.add(linkTo(methodOn(Application.class).getApplication()).withSelfRel());
        applicationResource.add(linkTo(methodOn(ProfileController.class).getProfiles()).withRel(PROFILES_REL));

        return applicationResource;
    }

}
