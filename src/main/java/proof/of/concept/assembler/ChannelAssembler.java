package proof.of.concept.assembler;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;
import proof.of.concept.controller.ChannelController;
import proof.of.concept.controller.ProfileController;
import proof.of.concept.model.Channel;

import java.util.Collection;

import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static proof.of.concept.assembler.ProfileAssembler.PROFILE_REL;

@Component
public class ChannelAssembler {

    public static final String CHANNELS_REL = "channels";

    public Resource<Channel> toResource(Channel channel) {
        Resource<Channel> resource = new Resource<>(channel);

        resource.add(linkTo(methodOn(ChannelController.class).getChannelByKey(channel.getProfileKey(), channel.getKey())).withSelfRel());
        resource.add(linkTo(methodOn(ProfileController.class).getProfileByKey(channel.getProfileKey())).withRel(PROFILE_REL));

        return resource;
    }

    public Resources<Resource<Channel>> toResources(Collection<Channel> channels) {
        return new Resources<>(channels
                .stream()
                .map(this::toResource)
                .collect(toList())
        );
    }
}
