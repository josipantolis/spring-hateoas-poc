package proof.of.concept.assembler;

import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;
import proof.of.concept.controller.ProfileController;
import proof.of.concept.controller.ReviewController;
import proof.of.concept.model.ReviewStats;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static proof.of.concept.assembler.ProfileAssembler.PROFILE_REL;

@Component
public class ReviewAssembler {

    public static final String REVIEW_STATS_REL = "review_stats";

    public Resource<ReviewStats> toResource(ReviewStats reviewStats) {
        Resource<ReviewStats> resource = new Resource<>(reviewStats);

        resource.add(linkTo(methodOn(ReviewController.class).getReviewStats(reviewStats.getProfileKey())).withSelfRel());
        resource.add(linkTo(methodOn(ProfileController.class).getProfileByKey(reviewStats.getProfileKey())).withRel(PROFILE_REL));

        return resource;
    }
}
