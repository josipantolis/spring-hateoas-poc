package proof.of.concept.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;
import proof.of.concept.controller.ChannelController;
import proof.of.concept.controller.ProfileController;
import proof.of.concept.controller.ReviewController;
import proof.of.concept.hal.FatResource;
import proof.of.concept.hal.FatResourceBuilder;
import proof.of.concept.model.Profile;
import proof.of.concept.service.ChannelService;
import proof.of.concept.service.ReviewService;

import java.util.Collection;

import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static proof.of.concept.assembler.ChannelAssembler.CHANNELS_REL;
import static proof.of.concept.assembler.ReviewAssembler.REVIEW_STATS_REL;
import static proof.of.concept.hal.EmbeddableLinkBuilder.assembleResourceUsing;
import static proof.of.concept.hal.FatResourceBuilder.buildFromContent;

@Component
public class ProfileAssembler {

    public static final String PROFILE_REL = "profile";

    @Autowired
    private ChannelService channelService;

    @Autowired
    private ChannelAssembler channelAssembler;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ReviewAssembler reviewAssembler;

    public Resource<Profile> toResource(Profile profile) {
        String key = profile.getKey();
        return getProfileBuilder(profile, key).buildWithoutEmbedded();
    }

    public FatResource<Profile> toFatResource(Profile profile) {
        String key = profile.getKey();
        return getProfileBuilder(profile, key).buildWithEmbedded();
    }

    public Resources<Resource<Profile>> toResources(Collection<Profile> allProfiles) {
        return new Resources<>(allProfiles
                .stream()
                .map(this::toResource)
                .collect(toList())
        );
    }

    private FatResourceBuilder.FinalBuilder<Profile> getProfileBuilder(Profile profile, String key) {
        return buildFromContent(profile).linkingTo(
                linkTo(methodOn(ProfileController.class).getProfileByKey(key)).withSelfRel(),
                assembleResourceUsing(channelAssembler::toResources)
                        .retrieveDataUsing(() -> channelService.findByProfileKey(key))
                        .andLinkTo(linkTo(methodOn(ChannelController.class).getChannels(key)).withRel(CHANNELS_REL)),
                assembleResourceUsing(reviewAssembler::toResource)
                        .retrieveDataUsing(() -> reviewService.findByProfileKey(key))
                        .andLinkTo(linkTo(methodOn(ReviewController.class).getReviewStats(key)).withRel(REVIEW_STATS_REL))
        );
    }
}
