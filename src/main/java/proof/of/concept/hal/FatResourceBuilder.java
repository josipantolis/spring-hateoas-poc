package proof.of.concept.hal;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import rx.Observable;
import rx.functions.Func2;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class FatResourceBuilder {

    public interface LinksBuilder<T> {
        FinalBuilder<T> linkingTo(Link... linkingTo);
    }

    public interface FinalBuilder<T> {
        FatResource<T> buildWithEmbedded();
        Resource<T> buildWithoutEmbedded();
    }

    public static <T> LinksBuilder<T> buildFromContent(T content) {
        return linkingTo -> new FinalBuilder<T>() {
            @Override
            public FatResource<T> buildWithEmbedded() {
                return new FatResource<>(content, retrieveEmbeddedResources(linkingTo), linkingTo);
            }

            @Override
            public Resource<T> buildWithoutEmbedded() {
                return new FatResource<>(content, null, linkingTo);
            }
        };
    }

    private static Map<String, ResourceSupport> retrieveEmbeddedResources(Link... links) {
        List<EmbeddableLink> embeddableLinks = Arrays.stream(links)
                .filter(link -> !link.getRel().equals(Link.REL_SELF))
                .filter(link -> link instanceof EmbeddableLink)
                .map(link -> (EmbeddableLink) link)
                .collect(toList());

        return retrieveAndAssemble(embeddableLinks);
    }

    private static Map<String, ResourceSupport> retrieveAndAssemble(List<EmbeddableLink> embeddableLinks) {
        Map<String, ResourceSupport> response = new HashMap<>(embeddableLinks.size());

        Observable.from(embeddableLinks)
                .map(link -> new KeValuePair<>(link.getRel(), link))
                .map(linkPair -> new KeValuePair<>(linkPair.key, linkPair.value.retrieveResource()))
                .forEach(linkPair -> response.put(linkPair.key, linkPair.value));

        return response;
    }

    private static class KeValuePair<K, V> {
        K key;
        V value;

        KeValuePair(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }


}
