package proof.of.concept.hal;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;

import java.util.Map;

public class FatResource<T> extends Resource<T> {

    private final Map<String, ResourceSupport> embedded;

    FatResource(T content, Map<String, ResourceSupport> embedded, Link... links) {
        super(content, links);
        this.embedded = embedded;
    }

    @JsonGetter("_embedded")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Map<String, ResourceSupport> getEmbedded() {
        return embedded;
    }
}
