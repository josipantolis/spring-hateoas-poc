package proof.of.concept.hal;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.function.Function;
import java.util.function.Supplier;

public class EmbeddableLinkBuilder {

    public interface LinkBuilder<T, R extends ResourceSupport> {
        EmbeddableLink<T, R> andLinkTo(Link andLinkTo);
    }

    public interface DataSupplierBuilder<T, R extends ResourceSupport> {
        LinkBuilder<T, R> retrieveDataUsing(Supplier<T> retrieveDataUsing);
    }

    public static <T, R extends ResourceSupport> DataSupplierBuilder<T, R> assembleResourceUsing(Function<T, R> assembleUsing) {
        return retrieveDataUsing -> andLinkTo -> new EmbeddableLink<>(andLinkTo, retrieveDataUsing, assembleUsing);
    }

}
