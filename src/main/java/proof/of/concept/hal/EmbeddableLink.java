package proof.of.concept.hal;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.function.Function;
import java.util.function.Supplier;

public class EmbeddableLink<T, R extends ResourceSupport> extends Link {

    private final Supplier<T> dataSupplier;
    private final Function<T, R> assembler;

    public EmbeddableLink(Link baseLink, Supplier<T> dataSupplier, Function<T, R> assembler) {
        super(baseLink.getHref(), baseLink.getRel());
        this.dataSupplier = dataSupplier;
        this.assembler = assembler;
    }

    R retrieveResource() {
        return assembler.apply(dataSupplier.get());
    }
}
