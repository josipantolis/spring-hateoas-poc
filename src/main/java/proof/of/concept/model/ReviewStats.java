package proof.of.concept.model;

public class ReviewStats {

    private final String profileKey;
    private final Integer oneStar;
    private final Integer twoStars;
    private final Integer threeStars;
    private final Integer foreStars;
    private final Integer fiveStars;

    public ReviewStats(String profileKey, Integer oneStar, Integer twoStars, Integer threeStars, Integer foreStars, Integer fiveStars) {
        this.profileKey = profileKey;
        this.oneStar = oneStar;
        this.twoStars = twoStars;
        this.threeStars = threeStars;
        this.foreStars = foreStars;
        this.fiveStars = fiveStars;
    }

    public String getProfileKey() {
        return profileKey;
    }

    public Integer getOneStar() {
        return oneStar;
    }

    public Integer getTwoStars() {
        return twoStars;
    }

    public Integer getThreeStars() {
        return threeStars;
    }

    public Integer getForeStars() {
        return foreStars;
    }

    public Integer getFiveStars() {
        return fiveStars;
    }
}

