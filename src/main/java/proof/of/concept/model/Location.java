package proof.of.concept.model;

public class Location {

    private final String address;
    private final Coordinates coordinates;

    public Location(String address, Coordinates coordinates) {
        this.address = address;
        this.coordinates = coordinates;
    }

    public String getAddress() {
        return address;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }
}
