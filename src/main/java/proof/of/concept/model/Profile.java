package proof.of.concept.model;

public class Profile {

    private final String key;
    private final String name;
    private final Location location;
    private final Double rating;

    public Profile(String key, String name, Location location, Double rating) {
        this.key = key;
        this.name = name;
        this.location = location;
        this.rating = rating;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public Double getRating() {
        return rating;
    }
}
