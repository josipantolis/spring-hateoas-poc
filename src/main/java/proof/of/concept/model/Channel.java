package proof.of.concept.model;

public class Channel {

    private final String key;
    private final String profileKey;
    private final String name;

    public Channel(String key, String profileKey, String name) {
        this.key = key;
        this.profileKey = profileKey;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public String getProfileKey() {
        return profileKey;
    }

    public String getName() {
        return name;
    }
}
