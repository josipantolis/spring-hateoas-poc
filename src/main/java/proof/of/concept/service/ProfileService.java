package proof.of.concept.service;

import org.springframework.stereotype.Service;
import proof.of.concept.model.Coordinates;
import proof.of.concept.model.Location;
import proof.of.concept.model.Profile;

import java.util.Arrays;
import java.util.Collection;

@Service
public class ProfileService {

    public Collection<Profile> findAll() {
        return Arrays.asList(
                getProfile("profileKey1"),
                getProfile("profileKey2")
        );
    }

    public Profile findOne(String key) {
        return getProfile(key);
    }

    private Profile getProfile(String key) {
        Location location = new Location("Zadarska 80, Zagreb, Croatia", new Coordinates(45.793552, 15.946755));
        return new Profile(key, "Profile for key " + key, location, 2.3);
    }

}
