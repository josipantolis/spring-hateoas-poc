package proof.of.concept.service;

import org.springframework.stereotype.Service;
import proof.of.concept.model.ReviewStats;

@Service
public class ReviewService {

    public ReviewStats findByProfileKey(String profileKey) {

        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new ReviewStats(profileKey, 2, 3, 2, 5, 6);
    }

}
