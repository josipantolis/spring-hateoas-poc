package proof.of.concept.service;

import org.springframework.stereotype.Service;
import proof.of.concept.model.Channel;

import java.util.Arrays;
import java.util.Collection;

@Service
public class ChannelService {

    public Channel findOne(String channelKey, String profileKey) {
        return getProfileKey(channelKey, profileKey);
    }

    public Collection<Channel> findByProfileKey(String profileKey) {
        return Arrays.asList(
                getProfileKey("channelKey1", profileKey),
                getProfileKey("channelKey2", profileKey),
                getProfileKey("channelKey3", profileKey)
        );
    }

    private Channel getProfileKey(String channelKey, String profileKey) {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Channel(channelKey, profileKey, "Channel for key " + channelKey);
    }

}
