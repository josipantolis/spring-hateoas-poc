package proof.of.concept.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import proof.of.concept.assembler.ChannelAssembler;
import proof.of.concept.model.Channel;
import proof.of.concept.service.ChannelService;

import java.util.Collection;

@Controller
@ResponseBody
@RequestMapping(value = "/profile/{profileKey}/channel", produces = MediaType.APPLICATION_JSON_VALUE)
public class ChannelController {

    @Autowired
    private ChannelService channelService;

    @Autowired
    private ChannelAssembler channelAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public Resources<Resource<Channel>> getChannels(
            @PathVariable("profileKey") String profileKey
    ) {
        Collection<Channel> channels = channelService.findByProfileKey(profileKey);
        return channelAssembler.toResources(channels);
    }

    @RequestMapping(value = "/{channelKey}", method = RequestMethod.GET)
    public Resource<Channel> getChannelByKey(
            @PathVariable("profileKey") String profileKey,
            @PathVariable("channelKey") String channelKey) {

        Channel channel = channelService.findOne(channelKey, profileKey);
        return channelAssembler.toResource(channel);
    }

}
