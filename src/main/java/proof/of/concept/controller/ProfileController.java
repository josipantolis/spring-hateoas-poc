package proof.of.concept.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import proof.of.concept.assembler.ProfileAssembler;
import proof.of.concept.model.Profile;
import proof.of.concept.service.ProfileService;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

@Controller
@ResponseBody
@RequestMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ProfileAssembler profileAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public Resources<Resource<Profile>> getProfiles() {
        Collection<Profile> allProfiles = profileService.findAll();
        return profileAssembler.toResources(allProfiles);
    }

    @RequestMapping(value = "/{key}", method = RequestMethod.GET)
    public Resource<Profile> getProfileByKey(
            @PathVariable("key") String key
    ) {
        Profile profile = profileService.findOne(key);
        return profileAssembler.toFatResource(profile);
    }

}
