package proof.of.concept.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import proof.of.concept.assembler.ReviewAssembler;
import proof.of.concept.model.ReviewStats;
import proof.of.concept.service.ReviewService;

@Controller
@ResponseBody
@RequestMapping(value = "/profile/{profileKey}/review", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ReviewAssembler reviewAssembler;

    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public Resource<ReviewStats> getReviewStats(
            @PathVariable("profileKey") String profileKey
    ) {
        ReviewStats reviewStats = reviewService.findByProfileKey(profileKey);
        return reviewAssembler.toResource(reviewStats);
    }
}
